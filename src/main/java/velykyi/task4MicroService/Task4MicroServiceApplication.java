package velykyi.task4MicroService;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Task4MicroServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(Task4MicroServiceApplication.class, args);
	}

}
