package velykyi.task4MicroService.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import velykyi.task4MicroService.dto.EmployeeDto;

import javax.validation.Valid;
import javax.validation.constraints.Size;

@Slf4j
@RestController
@RequestMapping("api/v1/employeesService")
@RequiredArgsConstructor
@Validated
public class EmployeeController {
    private final String MAIN_SERVICE_URL = "http://localhost:8080/api/v1/employees";

    @ResponseStatus(HttpStatus.OK)
    @PostMapping
    public EmployeeDto createEmployee(@Valid @RequestBody EmployeeDto employeeDto) {
        log.info("Create employee - - from task5MicroService - -: {}", employeeDto);

        return new RestTemplate()
                .postForObject(MAIN_SERVICE_URL, employeeDto, EmployeeDto.class);
    }

    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/{login}")
    public EmployeeDto getEmployee(@PathVariable String login) {
        log.info("Get employee - - from task5MicroService - -");

        return new RestTemplate()
                .getForObject(MAIN_SERVICE_URL + "/" + login, EmployeeDto.class);
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping(value = "/{login}")
    public ResponseEntity<Void> updateEmployee(@PathVariable String login, @RequestBody EmployeeDto employeeDto) {
        log.info("Update employee - - from task5MicroService - -: {}", employeeDto + " by login: " + login);
        new RestTemplate().put(MAIN_SERVICE_URL + "/" + login, employeeDto);

        return ResponseEntity.noContent().build();
    }

    @DeleteMapping(value = "/{login}")
    public ResponseEntity<Void> deleteEmployee(@PathVariable("login") @Size(min = 1, max = 2) String login) {
        log.info("Delete employee by login - - from task5MicroService - - : " + login);
        new RestTemplate().delete(MAIN_SERVICE_URL+ "/" + login);

        return ResponseEntity.noContent().build();
    }
}
